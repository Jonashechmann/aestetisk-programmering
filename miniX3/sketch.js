// deklare to variabler til senere brug 
let diameter = 100;
let angle = 0;

// jeg opsætter canvaset til at være tilpasset windowWidth og Heigjt
//Derudover kalder jeg anglemode til (degrees) for at sætte vinklerne i koden til grader i stedet for radianer
function setup() {
  createCanvas(windowWidth, windowHeight);
  angleMode(DEGREES);

}

//jeg kalder først en baggrund med høj gennemsigtighed for at skabe et spor efter bufferen 
// jeg bruger også translate til at rykke kordinatsættet til midten af canvaset 
//Jeg bruger så rotate(angle) til at rotere canvaset rundt om sin egen akse med en tidligere deineret vinkel. Dette værende 0 
function draw() {
  background(random(10,40),25);
  translate(width / 2, height / 2);
  rotate(angle);


// Dernæst defineres en ny variabel d, der beregnes ved at tilføje sin(frameCount) ganget med en fjerdedel af vinduets højde, til diameter. Dette betyder, at diameteren af cirklen varierer i takt med frameCount - windowHeight er med til at sikre at den altid er centreret og at den varierer fra en minimumsværdi på diameter til en maksimumsværdi, der er en fjerdedel af vinduets højde større end diameter. 
  let d = diameter + sin(frameCount ) * windowHeight/4;

// herefter laver jeg så et forloop der skal tegnet 100 ellipser og gør brug af cosinus og sinus til at skabe variationer i X og Y værdierne
// værdierne som jeg har valgt er mere eller midnre fundet ved trail and error, indtil jeg åbnede et resultat som jeg var tilfreds med.  
  for (let i = 0; i < 100; i++) {
    let x = cos(angle + i * 5) * (d / 1 - 20);
    let y = sin(angle + i * 5) * (d / 2 - 70);

// jeg tegner ellipsen med en stor stroke for at give cirklerne en nærmest glødende effekt. Jeg giver også ellipsen x og y variablen som dets koridinater så jeg sikre mig variation i ellipsernes position 
    fill (random(0,50));
    stroke(255);
    strokeWeight(40);
    ellipse(x, y, 25, 15);
   
  }

 // endegyldigt deklerer jeg angle variablen til at falde med en hver gang 
  angle -= 1; 
    
}

