## miniX3 

[Kør mit program](https://jonashechmann.gitlab.io/aestetisk-programmering/miniX3/index.html)

![](Throbber.png)

Hej og velkommen til min miniX3 opgave. I denne uge lød opgaven på at konstruerer en loading buffer (også kendt som en throbber) ved hjælp af p5.js. Mere specefikt lød opgaven således: 
"Use loops and any one of the transformational functions to redesign and program an “animated” throbber." 

### [Tag et kig i min source code](https://gitlab.com/Jonashechmann/aestetisk-programmering/-/blob/main/miniX3/sketch.js)

Jeg forsøgte mig i opgaven at lave en så simpel som mulig kode, mens samtidig at opnå en tildfredstillende throbber. 
Min kode er i bund og grund utrolig simpelt og benytter sig hovedsageligt af de matematiske formler Sinus og Cosinus til at give nye abstrakte værdier til min kdoe af flere omgange. Dette gær den ved hjælp af et forloop som tegner 100 ellipser. (for et bedre overblik over koden anbefaler jeg at tjekke min source code)

Hvis man kigger på det temporale aspekt i min kode, ville man kunne se at den hovedsageligt er styret af frameCount. Dette vil sige antallet af frames i sekundet. I dette tilfælde værende 60. Dette vil altså også sige at min framCounts værdi efter 60 sekunder altid vender tilbage til startværdien - dette værende 0. 
jeg eksperimenterede blandt andet med at halvere frameCount - for at skabe en langsomere throbber, men dette blev i længden mere frostrerende en tildfredstillende. 
Hvilket også spiller godt ind i læst litteratur i kurset indtil videre. Thorbbere besidder en dualitet i spillet mellem underholdning og frustration. Alt efter hvad du er i gang med og hvornår throbber bliver brugt - vil din opfattelse af den ændre sig.

Kommer throbberen midt i din Netflix serie, er du nok mere frustreret end hvis den kommer som resultat af din beslutning om at uplade et billede til 
instagram. Throbberen fungere og benytter sig af samme computationele tid i begge scenerier, men menneskets subjektivitet i interaktionen kan altså skabe splid - netop dette nævnes også i Lammarant teksten: "Emotions depend to a large extent on expectations, or the potential coming of a future event." (Lammerant, s. 88, 2018). 
Throbberen er et billede på computeren arbejder, men hvis individets temporale forståelse for computeren ikke stemmer over ens med computeren selv - kan der opstå frustrationer.

Det er derfor vigtigt når man møder en throbber i hverdagen at reflektere over er, at selvom der  som udgangspunkt ikke er nogen indikation på hvornår "loadlingen" er færdig, er det ikke lig med at softwaren ikke virker, men at throbberen er en indikation på at computeren arbejder - dette bliver også beskrevet i grundbogen 
"It shows that a certain operation is in progress, but what exactly is happening, and how long this will take, is not at all clear. There is no indication of progress or status as is the case with a progress bar, for instance. We see the icon spinning, but it explains little about what goes on in the background or about timespan." (Soon & Cox, s. 74, 2020)
I relation til netop mit program kunne dette evt løses med en simpelt loading bar, som jeg blandt andede implementerede i min miniX2, men en sådan løsning ville ikke altid løse problemet, men blot fremprovokere yderligere frustration. Et eksempel på dette kunne være hvis netflix implimenterede en loading bar - man kunne argumentere for at dette blot ville skabe yderligere frustation hvis laoding bar'en ikke rygtede sig tilstrækkeligt hurtigt i forhold til individits egen tidslige fornemmelse. 

Dette spiller også ind i spørgsmålet: "The question if computer programs have a sense of ending (the Halteproblem)..." (Wolfgang, s. 1, 2009). Som omhandler hvorvidt om computeren har en forsteålse for en slutning.
Det vil altså sige at det ikke er muligt at skrive en generel algoritme, der kan afgøre, om et vilkårlig computerprogram vil ende (holde op med at køre) eller køre for evigt.  I netop mit program er kodens temporalitet for eksempel et endeløst loop, som ikke slutter før koden lukkes. Det er altså værd at stille spørgmål til hvorvidt en computer overhovedet har en temporel forståelse for verdnen.


litteraturliste:
- Wolfgang Ernst, “‘... Else Loop Forever’: The Untimeliness of Media,” (2009)
- Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018),
- Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96

