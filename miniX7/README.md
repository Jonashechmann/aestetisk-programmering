## Genbesøg af miniX6 - Det nu umulige spil
[RunMe](https://jonashechmann.gitlab.io/aestetisk-programmering/miniX7/index.html)

[source code](https://gitlab.com/Jonashechmann/aestetisk-programmering/-/blob/main/miniX7/sketch.js)

[source code (cachter)](https://gitlab.com/Jonashechmann/aestetisk-programmering/-/blob/main/miniX7/catcher.js) 

[source code (fallingObject)](https://gitlab.com/Jonashechmann/aestetisk-programmering/-/blob/main/miniX7/fallingObject.js)

![](miniX7.png)


### Hvilken miniX har du valgt?
Jeg har valgt at genbesøge min sidst udarbejdet miniX dette værende miniX6, hvori jeg lavede et simpelt spil. Dette har jeg gjort, da jeg følte at jeg sad tilbage med en uløst fornemmelse, da jeg afleverede opgaven - denne fornemmelse var en jeg fik ved både programmet såvel som min reflektion omkring det. Jeg har derfor valgt at genbesøge denne opgave for at færdiggøre løse ender såvel som kaste et nyt udvidet perspektiv ned over mit spil.


### Hvilke ændringer har du lavet og hvorfor?
Jeg har faktisk kun lavet to meget simple ændringer i min kode, dog har disse to ændringer haft en uhyre stor effekt på hvordan interaktionen med spillet opleves - Hvilket jeg vil reflektere yderligere over senere.

Første ændring jeg lavede i mit spil, var tilføjelsen af en live score, samt en endegyldig score når man tabte. Dette har jeg tilføjet, da jeg ville prøve at tage spillet i en ny retning. Den nye retning kommer efter stræben på at skabe et mere kompetitivt spil, som ikke blot har tidsfordriv som mål, men også giver en tilskyndelse for at opnå en højere score end du gjorde sidst. Denne tilføjelse følte jeg var nødvendig, da spillet førhen til tider følte meningsløst og at der som følge af dette opstod en mangel på motivation til at fortsætte.

Det næste jeg tilføjede, var en dynamisk sværhedsgrad i spillet som steg i takt med, at du gjorde fremskridt i spillet. Denne tilføjelse var igen for at styrke det kompetitivt element i spillet, samtidig med at det styrkede motivationen til at spille det, da spillet førhen havde en meget statisk progression.

Spillets visuelle udtryk forbliver næsten identisk til hvad det var førhen, dog formår disse to ændringer som nævnt at give spillet en særdeles anden interaktions oplevelse. Netop dette vil jeg i næste afsnit reflektere yderligere omkring.


### Spillet i relation til digital kultur
##### Objekt orienteret design
Jeg har i min Source Code markeret den nye og det er bemærkelsesværdigt hvordan 14 linjer kode, kan ændre spiloplevelsen på så drastisk vis.

Bemærkelsesværdigt er også observationen af, at ingen af disse ændringer i koden falder under hverken min "catcher" eller min "fallingObject" klasser. Man kan med denne observation argumentere for en af objekt orienteret kodes helt store styrker. Man ser her hvordan komplette udtryk kan ændres ved blot få linjer kode, grundet klassernes allerede definerede elementer. Jeg har ikke ændret i klassen, men blot fodret "Draw" funktionen med ny information, til hvordan den skal interagere med objekterne.

Det er netop her man ser styrken bag objekt orienteret kode, samt dets indflydelse på moderne software design. Havde mit spil bestået af udelukkende globale og lokale variable ville disse ændringer være væsentligt mere komplekse. Dette ville i en kommerciel globaliseret verden ikke blot lede til yderligere arbejde, men også forsinkelser, forviring og rod på tværs af afdelinger (Fuller & Goffey, 2017), samt et større Co2 aftryk som resultat af en større filstørrelse på den endegyldige kode. 


##### Spillets udtryk (temporalitet)
Ydermere ser man som sagt store ændringer i interaktionsoplevelsen med spillet. Mit spil drog i tidligere udgave inspiration fra Google Chromes "Dino" spil - som man i høj grad kunne argumentere fungerer som en interaktiv throbber, hvilket også var ideen bag min daværende udgave af spilet.

Dette udtryk kom til syne i blandt andet udladelsen af en score, såvel som en statisk progression i sværhedsgrad - hvilket som tidligere nævnt resulterede i en til tider kedelig spiloplevelse, men samtidig formåede at tjene dets formål - at distrahere brugeren fra de computationelle processer som foregik under overfladen. Spillet fungerede nærmest som en tidsboble, der var ingen indikation på tid, score eller hastighed som fortalte dig, at tid passerede, blot en cirkulær progression med et tilfredsstillende lydbillede.

Temporaliteten ændre sig i den nylige revision af spillet gevaldigt. Spillet er nu hektisk. I takt med at du "fanger" et objekt bliver det næste objekt hurtigere og hurtigere. Parret med en tilfældig x værdi, samt en statisk udvikling i "cachter" klassens hastighed - bliver spillet til uundgåeligt endegyldigt umuligt.

Temporaliteten formår altså at ændre sig voldsomt, ved blot få tilføjelser til koden og dertil spillet, men som Lammerant rigtigt nok argumentere for i kapitlet "How humans and machines negotiate experience of time" fra The Techno-Galactic Guide to Software Observation (2018), så er tid en subjektiv menneskeskabt konstellation, som bliver påvirket af en række af forskellige faktorer: "the experience of time is a conglomerate of different experiences" (Lammerant, 2018, S.89).

I dette tilfælde formår 14 linjer kode at ændre din tidslige opfattelse radikalt.

### Litteratur 

- Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
- Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018), https://monoskop.org/log/?p=20190
- Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). 

