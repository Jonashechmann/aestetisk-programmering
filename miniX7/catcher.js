class Catcher {
    constructor(x, y, width, height) {
      this.x = x;
      this.y = y;
      this.width = width;
      this.height = height;
    }
  
    move(step) {
      this.x += step;
      this.x = constrain(this.x, this.width/2, width - this.width/2);
    }
  
    display() {
      rectMode(CENTER);
      rect(this.x, this.y, this.width, this.height);
    }
  }  