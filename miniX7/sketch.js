let fallingObject;
let catcher;
let score = 0; // sætter score til 0 
let speed = 2; //sætter start hastigheden på objektet der falder
let speedfaster = 1 // sætter det første hastighedspring til 1

function preload(){
  ding = loadSound("ding.mp3");
  pling = loadSound("pling.mp3");
  gameOver = loadSound("gameOver.mp3");
  blurr = loadImage("blur.gif");
  drone = loadSound("drone.wav");
}
function setup() {
  createCanvas(windowWidth, windowHeight);
  generateFallingObject();
  noStroke()
  catcher = new Catcher(width/2, height-50, 100, 20);

}

function draw() {
  background(blurr,10);
  fallingObject.move();

  if (fallingObject.hits(catcher)) {
    generateFallingObject(); 
    pling.play(); 
    
    //score vokser med 1 hver gang den rammer 
    score++; 
    //får hastigheden den vokser med til at vokse hver gang
    speedfaster+= 0.2;

    fallingObject.increaseSpeed(speedfaster); // sæt hastigheden op med speedfaster variablens nye værdi
  }
  if (fallingObject.isOffScreen()) {
    //generateFallingObject();
    noLoop();
    textAlign(CENTER);

    textSize(100);
    textStyle(BOLD);
    fill(255)
    text("game over", width/2, height/2);
    gameOver.play(); 

    //viser den endelige score den endelige score
    textSize(50);
    fill(255);
    text("final score: " + score, width/2, height/2 + 50);
       
  }

  catcher.display();
  fallingObject.display();
  
  //viser din nuværende score
  textSize(30); 
  fill(255);
  textAlign(CENTER);

  text("Score: " + score, width/2, height/6);
}


function generateFallingObject() {
// indskriver at hvert nyt variabel skal bruge "speed" til at bestemme dens hastighed
  fallingObject = new FallingObject(random(width), 0, 20, 20, speed);
}

function keyPressed() {

  if (keyCode === LEFT_ARROW) {
    catcher.move(-100);
    ding.play(); 
  } else if (keyCode === RIGHT_ARROW) {
    catcher.move(100);
    ding.play(); 
  }
}



class Catcher {
  constructor(x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  move(step) {
    this.x += step;
    this.x = constrain(this.x, this.width/2, width - this.width/2);
  }

  display() {
    rectMode(CENTER);
    rect(this.x, this.y, this.width, this.height);
  }
}


class FallingObject {
  constructor(x, y, width, height, speed) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.speed = speed;
  }

  move() {
  
    this.y += this.speed+2;
  }

  increaseSpeed(amount) {
    this.speed += amount;
  }


  isOffScreen() {

    return (this.y > height);
    
  }

  hits(catcher) {
  
    let halfWidth = this.width;
    let halfHeight = this.height;
    if (this.y + halfHeight > catcher.y+20 - halfHeight &&
        this.x > catcher.x-40 - halfWidth &&
        this.x < catcher.x+40 + halfWidth) {
      return true;
    } else {
      return false;
    }
  }
  display() {

    rectMode(CENTER);
    rect(this.x, this.y, this.width, this.height);
  }

}