class FallingObject {
    constructor(x, y, width, height, speed) {
      this.x = x;
      this.y = y;
      this.width = width;
      this.height = height;
      this.speed = speed;
    }
  
    move() {
    
      this.y += this.speed+2;
    }
  
    increaseSpeed(amount) {
      this.speed += amount;
    }
  
  
    isOffScreen() {
  
      return (this.y > height);
      
    }
  
    hits(catcher) {
    
      let halfWidth = this.width;
      let halfHeight = this.height;
      if (this.y + halfHeight > catcher.y+20 - halfHeight &&
          this.x > catcher.x-40 - halfWidth &&
          this.x < catcher.x+40 + halfWidth) {
        return true;
      } else {
        return false;
      }
    }
    display() {
  
      rectMode(CENTER);
      rect(this.x, this.y, this.width, this.height);
    }
  
  }