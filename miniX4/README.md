## miniX4 - Jonas Hechmann 

[RunME](https://jonashechmann.gitlab.io/aestetisk-programmering/miniX4/index.html)

[Tag et kig i min source code](https://gitlab.com/Jonashechmann/aestetisk-programmering/-/blob/main/miniX4/sketch.js)

![](pic2.png)
![](pic1.png)

### DATA CAPTURE HIDDEN IN PLAIN SIGHT

##### CAPTURE ALL PITCH

Is the extreme scenario of complete data collection of all of our information something that should have raised concerns way before it did? In recent times, plenty of scandals surrounding data collection have surfaced and raised questions around what data websites and software alike gather from you just accepting the terms of use - or more commanly reffered to ass "Cookies". 

"DATA CAPTURE HIDDEN IN PLAIN SIGHT" is a project created to potray the information overload that these agreements often utalize, in order to confuse, demotivate or distract the users from reading the entire agreement. 
The program features a central webcam. The webcam serves no purpose but to personalize the very real consequenses in relation to the words and sentences that become visable when the spacebar is pressed. This action reveals an array of data, that you through the agreement of cookies, often gives the host access too. 

The webcam in this context is used to show that it truely is YOU that gives the computer acess to this information - but then again, what other option do you have? Certainly not using the website and closing it is out of question? 



##### Beskrivelse af mit program og syntakser
Mit program er som tidligere beskrevet en artistisk illustration af alt den data som vi egentlig selv giver computeren lov til at indsamle. 
For at lave pogrammet har jeg hovedsageligt gjort brug af Webcam Capture, Arrays og If statements. Dette kan ses nærmere på ved at tage et kig i min source code - hvilket jeg vil opfordre til at gøre.

##### Refleksion om dataindsamling 
Mens koden i sig selv ikke indsamler nogen data, kan den give anledning til at reflektere over, hvordan software generelt kan indsamle og bruge data.

Dataindsamling er en vigtig del af mange software-applikationer, og det kan være både nyttigt og bekymrende. På den ene side kan indsamling af data hjælpe med at forbedre brugeroplevelsen og gøre software mere effektivt og personligt tilpasset. På den anden side kan det også være en trussel mod privatliv og sikkerhed, hvis data indsamles og bruges på en måde, der krænker brugerens rettigheder og anonymitet.

Det er ikke svært at forestille sig, hvordan et lignende program kunne indsamle data fra brugeren. For eksempel kan kameraet på en computer bruges til at indsamle billeder eller videooptagelser, der kan indeholde personlig information om brugeren eller deres omgivelser. Hvis denne type data blev indsamlet og gemt på computeren eller en ekstern server, ville det være en potentiel trussel mod brugerens privatliv. Derudover indsamler softwaren også personlige oplysninger så som IP og operativsystem. 

Lignende bekymringer gælder også for andre former for dataindsamling, som kan ske i baggrunden, mens brugeren bruger software. For eksempel kan software indsamle oplysninger om brugerens adfærd, interaktioner og præferencer og bruge denne information til at forbedre sine funktioner eller til at målrette reklamer eller markedsføring.

Selvom denne type indsamling kan hjælpe med at forbedre brugeroplevelsen, kan det også føles invasiv og krænkende, hvis brugerne ikke er bevidste om, at deres data indsamles og bruges på denne måde.
I det hele taget er dataindsamling et komplekst spørgsmål, der kræver afvejning af forskellige faktorer, herunder brugerens privatliv, softwarens funktionalitet og udviklerens intentioner. Mens denne kode i sig selv ikke er bekymrende, kan den stadig give anledning til at reflektere over, hvordan dataindsamling kan påvirke vores digitale liv og hvilke forholdsregler vi kan tage for at beskytte vores privatliv og sikkerhed. Specielt i en tid med øget "Datafication" (Mejias,Couldry, 2019)

På mange måder er vi blevet for gode til blot at sige "accepter" til cookies når vi først møder det obligatoriske cookie vindue, måske vi i fremtiden burde læse den nærmere igennem? 
