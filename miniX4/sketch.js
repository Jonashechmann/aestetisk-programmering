
//Opstiller en array der skal bruges til at udvælge en random promt fra listen og vise den på skærmen
var tekst =[ "Your current location: ","Number of photos in your Pictures folder","Default browser name: ","Size of your hard drive: ","Amount of RAM on your computer: ","Your computer's IP address: ","Operating system name: ","Operating system version: ","Graphics card name: ","Current CPU usage: "]
//Opstiller en variabel så jeg senere hen kan vise teksten igen ved at trykke på space en gang yderligere.
var visible = false;

//opstiller et canvas 
function setup() {
  createCanvas(500, 500);

  //webcam capture 
  capture=createCapture(VIDEO);
  capture.size(250,250);
  capture.hide();
}

function draw() {
  
//Tegner en baggrund med lav oppacity så teksten fader langsomt ud efter nogle frames
  background(random(0,10),10);

  // Hvis "visible" er sand, så udføres følgende kode:
  // Vælg en tilfældig tekstprompt fra "tekst" arrayet.
  // Placer teksten tilfældigt på skærmen med en tilfældig farve.
 if (visible) {
    textAlign(CENTER);
    textFont('Times New Roman');
    textStyle(BOLD); 
    var randomPhrase = tekst[floor(random(0, tekst.length))];
    fill(200,55,10);
    text(randomPhrase, random(0,width), random(0, height));
  }

  //tegner en rektangel der fungere som ramme for webcamet
  rectMode(CENTER);
  fill(200,55,10)
  rect(width/2,height/2,260,260)

  //tegner slutteligt webcamet så det står forrest på mit canvas
  image(capture,width/2,height/2,250,250);
  imageMode(CENTER);
  
}

// "visible" variablen beskriver i "keyPressed" funktionen, at hvis "visible" er sand, vil koden køre "draw" funktionen gentagne gange ved hjælp af "loop" funktionen, indtil brugeren igen trykker på mellemrumstasten. Hvis "visible" er falsk, vil koden stoppe med at køre "draw" funktionen ved hjælp af "noLoop" funktionen, og en "GRAY" filter vil blive anvendt på skærmen for at gøre alt i gråtoner.
function keyPressed(){
  if(keyCode===32){
    visible = !visible;
    if (visible) {
      loop();
   
    } else {
      noLoop();
      filter(GRAY);
    }
  }
}
