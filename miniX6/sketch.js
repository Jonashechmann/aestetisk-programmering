let fallingObject;
let catcher;


function preload(){
  ding = loadSound("ding.mp3");
  pling = loadSound("pling.mp3");
  gameOver = loadSound("gameOver.mp3");
  blurr = loadImage("blur.gif");
  drone = loadSound("drone.wav");
}
function setup() {
  createCanvas(windowWidth, windowHeight);
  generateFallingObject();
  noStroke()
  catcher = new Catcher(width/2, height-50, 100, 20);

}

function draw() {
  background(blurr,10);
  fallingObject.move();

  if (fallingObject.hits(catcher)) {
    generateFallingObject(); 
    pling.play(); 


  }
  if (fallingObject.isOffScreen()) {
    //generateFallingObject();
    noLoop();
    textAlign(CENTER);

    textSize(100);
    textStyle(BOLD);
    fill(255)
    text("game over", width/2, height/2);
    gameOver.play(); 
       
  }

  catcher.display();
  fallingObject.display();
}


function generateFallingObject() {

  fallingObject = new FallingObject(random(width), 0, 20, 20, 2);
}

function keyPressed() {

  if (keyCode === LEFT_ARROW) {
    catcher.move(-100);
    ding.play(); 
  } else if (keyCode === RIGHT_ARROW) {
    catcher.move(100);
    ding.play(); 
  }
}



class Catcher {
  constructor(x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  move(step) {
    this.x += step;
    this.x = constrain(this.x, this.width/2, width - this.width/2);
  }

  display() {
    rectMode(CENTER);
    rect(this.x, this.y, this.width, this.height);
  }
}


class FallingObject {
  constructor(x, y, width, height, speed) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.speed = speed;
  }

  move() {
  
    this.y += this.speed+2;
  }

  isOffScreen() {

    return (this.y > height);
    
  }

  hits(catcher) {
  // laver en hitbox og kører en bolien statement hvis den rammer hitboxen 
    let halfWidth = this.width;
    let halfHeight = this.height;
    if (this.y + halfHeight > catcher.y+20 - halfHeight &&
        this.x > catcher.x-40 - halfWidth &&
        this.x < catcher.x+40 + halfWidth) {
      return true;
    } else {
      return false;
    }
  }
  display() {

    rectMode(CENTER);
    rect(this.x, this.y, this.width, this.height);
  }

}