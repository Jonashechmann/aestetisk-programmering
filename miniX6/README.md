### miniX6 "Object Abstraction" - Jonas Hechmann Gejl 

[RunMe](https://jonashechmann.gitlab.io/aestetisk-programmering/miniX6/index.html)

[source code](https://gitlab.com/Jonashechmann/aestetisk-programmering/-/blob/main/miniX6/sketch.js)

![](game.png)

##### Hvordan spiller man spillet?
Spillet er egentligt utroligt simpelt. Det fungere på følgende måde. Et objekt genrerer på en tilfældig plads på x aksen og falder mod jorden. Nederst på skærmen er din "player" som er den du styrer. Den rykker du på tværs af x aksen for at gribe de faldende objekter, hvis ikke du formår at gøre dette, taber du spillet og en "Game Over" promt vil komme frem. Derudover har jeg inkodet en animeret baggrund, samt en smule lyddesign for at gøre spilelt lidt mere sjovt - da spillet ellers var meget ensformet. 

##### Hvordan fungere koden?
Koden er i takt med ugens tema om objekt orienteret programmering opbygget af to hovde "Klasser" som styrer alt logikken i spillet. Dette er gjort for at optimere koden fremfor at "Hard Code" alt logikken med variabler og for-loops.

Catcher-klassen repræsenterer objektet, som spilleren kan bevæge fra side til side for at fange den falende genstand. Denne klasse har fire attributter: x, y, width og height. x og y bestemmer positionen på skærmen, og width og height bestemmer størrelsen på objektet. Catcher-klassen har to metoder, move og display. move-metoden bevæger Catcher-objektet til højre eller venstre afhængigt af input fra spilleren og begrænser dens position til skærmens bredde ved hjælp af constrain-funktionen. display-metoden viser Catcher-objektet på skærmen som en rektangel.

FallingObject-klassen repræsenterer objektet, som falder ned på skærmen, og som spilleren skal forsøge at fange. Denne klasse har fem attributter: x, y, width, height og speed. x og y bestemmer positionen på skærmen, width og height bestemmer størrelsen på objektet, og speed bestemmer, hvor hurtigt objektet falder. FallingObject-klassen har tre metoder: move, isOffScreen og hits. move-metoden bevæger FallingObject-objektet nedad på skærmen. isOffScreen-metoden returnerer true, hvis FallingObject-objektet er faldet ud af skærmen, og false ellers. hits-metoden tager en Catcher-parameter og returnerer true, hvis FallingObject-objektet rammer Catcher-objektet og false ellers. hits-metoden beregner en "hitbox" omkring Catcher-objektet og sammenligner den med FallingObject-objektets position for at afgøre, om de rammer hinanden. FallingObject-klassen har også en display-metode, som viser objektet på skærmen som en rektangel.

Man kan altsp sige at de to klasser arbejder sammen for at skabe et simpelt spil. Objektorienteret programmering gør det altså muligt at opdele spillet i forskellige abstrakte enheder (klasser) og organisere deres interaktioner på en struktureret måde.


##### Reflektion om koden 

Indtil nu har jeg ofte følt, at kode tit er uoverskueligt og jeg har ligeledes i min egen tid tænkt på, hvordan enorme "Tripple A" spil formåede at overholde et overblik i deres kode. Dette giver med objekt orienteret kodning væstenligt bedere mening. Jeg formåede ikke at få min kode til at fungere hvis jeg yderligere prøvede at optimere min kode ved at opdele "Class'esne" i eksterne js. filer. Jeg indser dog også at netop dette er en essentiel del af kodning. 

Her tænker jeg meget specefikt tilbage på min tidlige teenage år hvor jeg ofte bruge timevis i forskellige spils filer, i håb om at kunne "modde" dem. Jeg indser nu, at disse filer på samme vis var opdelt i forskellige blokke af kode på tværs af mange hunrede js. filer, fremfor en stor js. fil, som vi indtil nu har arbejdet med. Jeg ser nu en væstenlig vigtighed i at holde orden i sin kode - dette er ikke blot for egen skyld, men også i et scenarie, hvor man arbejder på kryds af tværs af teams og personale. Netop dette bliver også beskrevet i Fuller og Goffey teksten præsenteret i denne uge: "Discussing the development of Smalltalk, Kay says: ‘object-oriented design is a successful attempt to qualitatively improve the efficiency of modelling the ever more complex dynamic systems and user relationships made possible by the silicon explosion’. - noget som jeg ikke tænkte over tidligere. Metoden er altså opstået på baggrund med en stigende tendens for kodning på tværs af store indutrier - eller som specefikt nævnt i citatet "the Silicon Explosion" som var explosionen i TECH industrien i USA - der krævede stigende kode kendskab og organisation  


##### Litteratur 

- Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
- Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). (Uploadet til BrightSpace)

