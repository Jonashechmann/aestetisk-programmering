## miniX5 - Jonas Hechmann Gejl 

### Generativ Kode 


[RunMe](https://jonashechmann.gitlab.io/aestetisk-programmering/miniX5/index.html)

![](noise.png)

##### Regelsæt, FOR-Loops og Conditional Statements 

- For er bedre overblik over programmet bedes man tage et kig på min [source code](https://gitlab.com/Jonashechmann/aestetisk-programmering/-/blob/main/miniX5/sketch.js)


Min generative kunst bruger tilfældighed til at skabe et dynamisk og evigt skiftende mønster. Brugen af støjfunktionen minder om den pseudotilfældige talgenerator (mere om dette senere), der blev brugt i "10 PRINT". I denne kode genererer støjfunktionen en værdi baseret på linjens position og tiden t, som derefter bruges til at bestemme farven og tykkelsen af linjen.

Koden skaber et gitter af diagonale linjer, der ændrer farve og tykkelse baseret på støjen, der genereres af støjfunktionen, som fungere som mine conditional statements. Hvis støjværdien er større end 0,5, tegnes en diagonallinje med variabel farve og tykkelse, og hvis den er mindre end eller lig med 0,5, tegnes en diagonal linje med en anden farve. Dette skaber et smukt og dynamisk mønster, der udvikler sig over tid.

Ligesom "10 PRINT" demonstrerer denne kode kraften i tilfældighed i generativ kunst og hvordan det kan bruges til at skabe komplekse og uforudsigelige mønstre. Ved at ændre parametrene for støjfunktionen, såsom skalaen eller oktaverne, kan koden skabe forskellige mønstre, der er lige så smukke og dynamiske.

Derudover inkluderer koden også brugerinteraktion som en lille bonus, ved at ændre baggrundsfarven til rød med 50% gennemsigtighed, når musen klikkes. Dette skaber et unikt interaktivt element, der giver brugeren mulighed for at påvirke det genererede mønster.

Koden besidder altså et rægelset samt condotional statements


##### Reflektsion om generativ kode
Meget af den litteratur som vi fik præsenteret i denne uge omhandlede den tilsyneladende ikke så "Random" natur som funktionen med samme navn besidder. For på mange måder er Random funktionen slet ikke random, dette bliver også beskrevet i "10 PRINT" teksten: "Digital computers are deterministic devices—the next state of the machine is determined entirely by the current state of the machine. Thus, computer-based random number generators are more technically described as pseudorandom number generators. (Monfort, et al. 2012, s. 144)


Man kan derfor stille spørgsmål til om computeren i virkelighed nogensinde opnår ægte tilfældighed, men er dette også virkeligt nødvendigt? Hvis den efterlignede tilfældighed som computeren skaber, er god nok til at skabe et uforudsigeligt output, som et menneske ikke kan afkode, så åbner computeren vel også en tilstrækkelig tilfældighed? I relation til generativ kode som et kunstværk er det måske netop dette argument som er mest centralt. Observere man min kode i timevis, år, år tier eller en hel livstid ville jeg mene, at man aldrig nogensinde ville kunne forudse hvordan de næste linjer ville blive tegnet. Set i lyset af dette, er illusionen af tilfældighed måske ikke lige så god som faktisk tilfældighed?


Samtidig ville man måske kunne argumentere for, at en person med adgang og kendskab til algoritmen som computeren benytter, ville kunne skabe en lignende algoritme der kunne forudse udkommet af computerens tilfældigheds illusion. Ydermere kunne denne proces af tillæring af computerens algoritme udføres af en anden computer i form af machine learning og derved effektivisere processen. For hvis en computer kan udregne tilfældige værdier, så kan en anden computer vel også forudse dem?

Kigger man igen på den kunstneristiske del af generativ kode, så er noget som ejerskab noget man kan tage til diskussion. Et af de etiske spørgsmål, der diskuteres i Soon og Cox teksten (2020), er spørgsmålet om ophavsret og ejerskab. Da auto-genereringsprocesser ofte indebærer, at kunstneren giver en vis grad af kontrol til algoritmer og systemer, kan det være svært at fastslå, hvem der er den retmæssige ejer af det endelige værk. Hvilket med kunstværkets udgivelse kan skabe udfordringer i forhold til rettigheder og kompensation.
Dette bliver yderligere forstærket med tanken om, at hvis en algoritme der fra menneskets perspektiv er tilfældig, hvordan kan kunstneren så have en reel kontrol over hvad programmet genere i fremtidige iterationer af værket?  



#####Litteratur Liste
- Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142
- Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, (Cambridge, MA: MIT Press, 2012), 119-146. 






