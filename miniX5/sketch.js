// Sæt tiden til 0
let t = 0;

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  // Sæt baggrunden til grå
  background(220);
  
  // Lav et gitter af linjer med intervaller på 10 pixels
  for (let x = 0; x <= width; x += 10) {
    for (let y = 0; y <= height; y += 10) {
      // Beregn en støjværdi baseret på positionen af linjen og tiden t
      let noiseVal = noise(x/100, y/100, t);
      // Hvis støjværdien er større end 0,5, tegn en skrå linje med varierende farve og tykkelse
      if (noiseVal > 0.5) {
        stroke(map(noiseVal, 0, 1, 0, 255));
        strokeWeight(map(noiseVal, 0, 1, 0, 5));
        line(x, y, x + 10, y + 10);
      } else {
        // Ellers tegn en diagonal linje med en anden farve
        stroke(map(noiseVal, 0, 0.5, 0, 255));
        line(x, y + 10, x + 10, y);
      }
    }
  }
  
  // Hvis musen er klikket ned, skift baggrundsfarven til rød med 50% gennemsigtighed
  if(mouseIsPressed === true) {
    background(255, 0, 0, 50);
  }
  
  // Øg tiden med 0,1 ved hvert frame for at animere støjværdierne
  t += 0.01;
}
