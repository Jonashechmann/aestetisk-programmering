## Mini X9 - Jonas, Carl, Jens og Jonas

[RuneMe (Giv lige koden 10sek til at loade)](https://jonashechmann.gitlab.io/aestetisk-programmering/miniX9/index.html)

[source code](https://gitlab.com/Jonashechmann/aestetisk-programmering/-/blob/main/miniX9/minix9.js)

![](minix9.png)

[json1](https://gitlab.com/Jonashechmann/aestetisk-programmering/-/blob/main/miniX9/kate1.json)
[json2](https://gitlab.com/Jonashechmann/aestetisk-programmering/-/blob/main/miniX9/kate2.json)
[json3](https://gitlab.com/Jonashechmann/aestetisk-programmering/-/blob/main/miniX9/kate3.json)

#### Beskrivelse af vores værk - HaiCODE


Et glimt på skærmen

Poesi fra det blå lys

Følelser vækkes

Værket HaiCODE et forsøg på at forene to vidt forskellige medier, henholdsvis den ældgamle japanske digtform haiku og det moderne kodesprog i form af p5.js. Et umage par som ved første øjekast, måske i realiteten har flere fællesnævnere end man umiddelbart skulle tro. I haikudigte prøver man efter en fast struktur på 5-7-5 stavelser, at formidle et budskab i så komprimeret et format som overhovedet muligt. På samme måde forsøger programmører også at komprimere deres kode og sætte det i system. I HaiCODE bliver poesiens og kodningens verden sammenflettet igennem en ‘haiku-digt-generator’ som generer haiku-digte, hvis betydning og budskab konstant ændres for hver gang der generes et digt. Samtidig er haikudigtets stramme og faste struktur overført til source-koden for at bygge bro mellem de to verdener gennem struktur og poesi.


#### Hvordan virker programmet og hvilke syntakser har vi brugt?
Vores program er en haiku generator. Et haiku er et digt, der består af tre strofer, to strofer af 5 stavelser og et af 7 stavelser. Vi har brugt tre JSON filer, som hver især indeholder de forskellige strofer hvor vores sketch tilfældigt vælger mellem de forskellige strofer, og på den måde kontinuerligt danner nye haiku digte. Ord får først forståelig betydning i kraft af hinanden, hvilket kræver at der er den korrekte syntaks. På den måde minder ord, poesi og dets lige meget om kodning. Kodning er først brugbar når den står i den korrekte syntaks og er kun i meget sjældne tilfælde læsbar når det står i dets binære form. 



Denne minix handlede også omkring brugen af .json filer - både organisationen, opbevaringen og hentningen af disse. Filerne skulle behandles som andre filer vi har skulle importerede og preloades og kobles til en variabel. Herefter kalder vi på dem ved hjælp af en random variabel som gør at der bliver valgt en tilfældig strofe fra den tilhørende .json fil. På denne måde bliver der skabt et nyt og tilfældigt haiku digt hver gang den bliver kørt

For at skabe et flow i strofens fremtræden skulle vi have dem til at blive vist forskudt, så der altså ville komme en linje af gange. For at opnå denne effekt benyttede vi os af en lav framerate på kun 10 frames i sekundet og så kaldte vi vores .json i 3 forskellige ``if`` statements som gjorde at de ville blive synlige når framecounten var så høj som vi ønskede den. På denne måde opnåede vi den ønskede temporale form i vores haiku generator.



#### Refleksioner / analyse af vores værk
 
At lære at kode er som at lære et nyt sprog, men med afsæt i Vocable Code, så er det en måde at skrive kode på, der forbinder det ellers så indviklet kodesprog med menneskesprog: “the code itself is a mixture of computer programming language and human language, and aims to expose the material and linguistic tensions between writing and reading…”(Soon & Cox, 2020). Når man taler om software, er det næsten altid kun selve det visuelle program, der bliver omtalt, mens source-koden bliver glemt. Men ved netop at give source-koden betydning, kan man give programmøren et talerør: ​​”This is where we hear the programmer’s voice.”(Soon & Cox, 2020). 
I vores egen kode har vi opsat koden som et haiku-digt. Dette er blevet gjort systematisk efter 5-7-5-reglen og giver ingen mening ift. almindeligt kodesprog, men gør samtidig at man ser source-koden i en ny kontekst: “These materials are not to be used logically in their objective relationships, but only within the logic of the work of art.” (Cox & McLean, s. 17) Foruden det, er Haiku også unikt inde for litteraturen, da den, ligesom kode, er meget regelbaseret. Man kan sige at der er et slags metalag, ved et program der aktivt skaber regelbaseret poesi gennem et regelbaseret kodesprog som Javascript. Vi er langt fra de første der har fået ideen om at lave en Haiku generator, der ved hjælp af forskellige datasæt programmerer dets eget haikudigte. 
Et andet spændende aspekt af poesi skabt af programmering, er spørgsmålet om, hvorvidt vi som programmører kan tage ejerskab af de digte der bliver skabt. Da vi har skrevet koden og fyldt json filerne med data ville man kunne argumentere for, at vi selv har dels ejerskab i værkets endegyldige udtryk. Dog kan det også argumenteres for at vi som programmører der gør brug af random funktion blot lader poesien være op til algoritmen. Hvem skal hæfte for digtene hvis de f.eks. bliver stødende? Er det os, eller koden selv, der bør kræve ejerskab. I vores program er det måske et nemt spørgsmål at svare på, da koden ikke er super kompleks og vi selv har skrevet de relativt få strofer, men så snart kompleksiteten højnes bliver det et langt svære spørgsmål at svare på. 




#### Referenceliste
- Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press,2013), 17-38. (Uploadet til BrightSpace)

- Soon Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186

