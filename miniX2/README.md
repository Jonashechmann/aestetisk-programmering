# miniX2 - Jonas Hechmann Gejl
## Emoji Generatoren 
[Kør programmet her!](https://jonashechmann.gitlab.io/aestetisk-programmering/miniX2/index.html)

![](emoji1.png)

![](emoji2.png)

### Hvad er programmet og hvordan fungere det?
[Tag et kig i Source coden](https://gitlab.com/Jonashechmann/aestetisk-programmering/-/blob/main/miniX2/sketch.js)

Selve udtrykket af programmet er egenligt meget ligetil og burde være nemt at følge for de fleste. Ved programmets opstart mødes man med en tæller og en loading "bar". Tællerenn tæller antallet af emojis på skærmen mens loading baren viser hvornår din egen tilfældigt genererede emoji er klar. 

Udover dette er der også en "Musik!" knap som man ved at trykke på kan få til at spille noget musik, samtidig med at man gør dette muliggør man en lydeefekt når din emoji er klar. Dette skyldes at Chrome og mange andre browsere har indtænnkt større sikkerhed i deres browsere. Hvilket har resulteret i at lyd ikke længere kan afspilles på en browser, hvis ikke brugeren har interegeret med hjemmesiden på en eller anden måde - dette var et problem jeg fandt frem til ved at bruge "undersøg" funktionen under testning. Her fik jeg et link der forklarede problemet. 

##### Hvilke syntakser har jeg brugt?
Programmet endte med at blive mere kompleks en først tiltænkt, men dette gjorde også at jeg dannede kendskab med en masse nye syntakser. Blandt andet hvordan man laver en loading bar og hvordan man ændrede i sin html fil til at kunne udnytte P5.sound biblioteket. Disse var alle syntakser som jeg fandt frem til gennem P5's refference system - som i denne opgave var en utrolig stor hjælp - og næsten udelukkende den eneste kilde jeg benytede mig af. 

Jeg har selfølgelig også benyttet mig at de geometriske syntakser kombineret med syntaksen "random()" til at skabe den endegyldige emoji som man får lavet. Jeg har specielt fået meget øvelse i hhv hvordan man bruger de her to syntakser i kombination med variabler, men også i høj grad hvor vigtig strukturen for din kode er for udfaldet. Grundet de mange bevægelse elementer i min kode, samt nulstillingen af dem til slut, mødte jeg mange problemer med rækkefølgen på min kode. Jeg lærte at bare fordi koden var rigtig, var det ikke nødvendigvis lig med, at den fungerede som du forventede at den ville. Her var VSC live feature til stor hjælp - da jeg kunne fejlfinde i "realtime". I takt med denne opdagelse endte jeg også med at opdage "Loop() og noLoop()" syntakserne igennem refference systemet - jeg tror at disse i fremtiden vil være nogen jeg vil gøre væstenligt mere brug af. Da de giver dig en rigtig god kontrol over din "draw" - fremfor blot 60fps lige efter hinadnen konstant.  

#### Hvordan står mit program i relation til pensum indtil nu?
Programmet var egentlig først udtænkt som værende et spil på hvordan løsningen på inklusivitet indtil videre har været udtænkt. Indtil videre har løsningen blot været at tilføje flere og flere emojis, i håb om at indtænke alle - det viser sig dog at dette ikke nødvendig er udfaldet. Netop dette bliver bragt op i "Modyfying the Universa" (Abbing, Pierrot, Snelting) hvor de forklare hvordan tilføjelsen af flere og flere emojis blot tydeliggører problemet, mens det samtidig gør valget af hvilken emoji man vil bruge sværere. 
“...it does not make sense to fix these issues... by adding yet
more variables, as the mechanism of varying
between binary oppositions itself is fundamentally
flawed (Abbing, Pierrot, Snelting, p. 46).”


Det er netop her essensen i min kode er. Der er så mange mulgiheder for hvilken emoji du kan vælge og selv med de bedste intentioner, er det med disse emojis, muligt at støde individer eller befolkningsgrupper. Derfor tænkte jeg mit program som en universel løsning på problemet, der præsenterede en mere neutral emoji, samtidig med at den valgte en for dig. 
Men det var netop også i denne del af mit program hvor jeg blev præsenteret med endnu en essentiel pointe fra teksten. Hvordan programmere mand den perfekte neutrale emoji? Jeg fandt mig selv stille samme sprøgsmål: “Who or what is the template for this “universal” character?" (Abbing, Pierrot, Snelting, p. 42). 

Jeg opdagede at det krævede enorme mængder af koder at opnå et sådan universelt "blueprint" for emoji. Genreliseringen i kode var her for mig meget tydelig. 
