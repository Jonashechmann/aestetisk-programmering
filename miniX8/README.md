## MiniX8 – Gruppeaflevering - Jonas, Carl, Jens og Jonas 

#### De andre gruppemedlemmers flowcharts
[Jonas D.](https://gitlab.com/jonas61005/aestetiskprogrammering/-/tree/main/minix8)
[Carl](https://gitlab.com/carlvestbo/aestetiskprogrammering/-/tree/main/MiniX8)
[Jens](https://gitlab.com/jens5376/aestetisk-programmering/-/tree/main/MiniX-%208)

#### Flowchart til min egen individuelle miniX

jeg har valgt at lave en flowchart over min miniX7 - hvilket værende min revuderet udgave af mit spil. I dette tilfælde opdagede jeg en af flowcharts helt store fordele. Jeg valgte at lave en flowchart som skulle fokusere på "Udviklingen" i spillet og jeg kunne nu med Flowchart se mangler i mit spil som jeg ikke tidligere havde tænkt på.

1. Spillet mangler en "Spil igen" knap 
2. Spillet kan ikke vindes, kun tabes. 


![](Spil.png)

#### Vores to ideer – ”Real Captcha” & “Virus”

-	“REAL-Captcha”
RealCaptcha er en interaktiv kunstinstallation, der forsøger at belyse AI’s indflydelse og betydning på vores hverdagsliv med eksempelvis Smart-assistens som ALEXA og chatbots som chatGPT. Kunstprojektet udformes ved at fungere som en omvendt ”smart personal assistent”, hvor rollerne mellem AI og menneske er byttet om, så mennesket bliver computerens personlige assistent. Dette bliver konkret vist ved at programmet først skal verificere at den er en computer, med inspiration fra googles ReCaptcha, før programmet kommer ind på selve chatsiden, hvor den efterfølgende vil stille brugeren x antal spørgsmål, indtil den er tilfreds med udbyttet af den menneskelig chatbot.

![](Captcha.png)


-	”Virus”
Virus er et program som har til formål at simulere, men frem for alt belyse hvordan en virus kan sprede sig på tværs af en befolkning. Vi har tænkt os at gøre dette ved at opstille to classes som skal fungere som vores smittede og vores ikke smittede. Disse to classes vil konstant tjekke efter kollisioner med hinanden og vis dette skulle ske, ville en transaktion af smitten ske og den førhen sorte cirkel vil blive rød og derved have samme egenskaber som de smittede. Efter de smittede har været smittet i et minut – vil programmet vælge om cirklen skal forsvinde (dø) eller blive sort igen (blive rask). Dette vil køre for evigt eller indtil virussen formår at dræbe alle cirkler.  


![](Virus.png)

#### What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?

Det kan være utroligt svært at skabe en overskuelig flowchat som en visuel repræsentation for en meget kompleks kode. Flowcharten kan hurtigt blive utrolig kompleks og derved overskygge dens egentlige funktion – værende en simpel og overskuelig fortolkning af din kode. Soon og Cox teksten præsenterer disse stridspunkter som to overordnede problematikker når man skal forvandle kode til en flowchart: 

1.	Translating programming syntax and functions into understandable, plain language.
2.	Deciding on the level of detail on important operations to allow other people to understand the logic of your program. 

(Soon, Cox, 2020)

#### What are the technical challenges facing the two ideas and how are you going to address these?

Det sværeste element i forhold til REAL-Captcha vil i høj grad være at efterligne en AI’s opførsel, da rollerne vil være byttet om. Derudover kan der opstå komplikationer med computerens verificeringen af at det rent faktisk er en AI. Vores tanke er at bruge CML-koden til at lave ansigtstracking som AI så skal ”trykke” på ansigtet, ligesom man normalt i en ReCaptcha skal trykke på de firkanter med lyskryds osv. Dette vil være teknisk svært hvis vi med machine learning faktisk lærer AI’en at genkende et menneskeansigt, men det kan også laves mere som et pseudo-element der bare tjekker en bestemt boks fra webcam hvor ansigtet typisk vil være.

Det sværeste element med Virus-programmet er det temporale element i koden ift. At lave forskellige tidsforståelser. Vi skal på en måde have skabt et internt ur i koden som skal time hvor længe cirklerne har været smittet – vi forventer at dette kan blive en udfordring da vi ingen tidligere erfaring har med sådan kode. Derudover forventer vi også at vi kan møde problemer ved de to klassers kollisions systemer. VI er dog klare over hvad der skal til for at få det til at virke, men kompleksiteten i sig selv forventer vi kan blive en udfordring 

#### In which ways are the individual and the group flowcharts you produced useful?

Det mest brugbare ved at gennemgå egen individuel kode, er at man får et bedre indblik i hvordan koden reelt set fungerer. Det kan være svært at visualiserer præcist hvordan kodens dele interagerer indbyrdes, hvilket flowchartet nemt demonstrerer. Dette kan virke ligegyldigt i simpel kode, men desto større og desto mere kompleks koden bliver kan man nemt miste overblikket. Hvad angår gruppens kode og flowchartet er det et effektivt værktøj til planlægningen da man på den måde får overskueliggjort de forskellige kode dele. 
