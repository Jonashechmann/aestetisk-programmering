## MiniX 10 - Jonas, Carl, Jens og Jonas 

### Style Transfer - Image 

![](1.png)
![](3.png)
![](2.png)

#### Hvilken eksempel kode har I valgt, og hvorfor?
Vi har valgt at sætte vores kode på Style Transfer algoritmen. Dette har vi gjort da vi føler det er en kode som vi kan snakke meget om, men også en kode som i den grad er meget relevant i nutidens dagsorden. Kunst og dertilhørende lignede visuelle grafiske, men  også i høj grad kreative felter har altid været områder som man har opfattet som værende unikke for den menneskelige hjerne. Senere år har dog set en ændring i dette. I takt med udviklingen i AI og deres dertilhørende træningssæt har vi i nyere år set flere og flere eksempler på AI’s som kan producere unik kunst ud fra simple ting såsom en tekstbaseret prompt eller et referencebillede.    

#### Har I ændret noget i koden for at forstå programmet? Hvilke?
Først og fremmest kiggede vi på det åbenlyse, som vi kunne ændre i vores kode. Dette var selvfølgelig input materialet, som vores AI skulle manipulere. Her kom vi blandt andet frem til limitation med filstørrelsen på input materialet. Vi arbejdede os frem til at dette, med stor sandsynlighed skyldtes, problemer med kompatibiliteten mellem træningsættets filstørrelse (pixel) og den data som vi satte ind. Hvilket på sin vis trække strenge til sidste uges tema og den uhyre stringente syntaks som følger kode - vi sikrede os derfor at vores input materiale var forståeligt for machine learning algoritmen. 
Dog fandt vi hurtigt ud af, at det var muligt at opskalere det endegyldige output som AI’en producerede. Dette gjorde vi ved at ændre pixel densiteten i variablen “newImage” fra 250x250 til 1000x1000. Dette resulterede dog i et endeligt output i bemærkelsesværdigt dårligt kvalitet, hvilket mest sandsynligt kommer fra, at AI’en som vi valgte at bruge, ikke er trænet til opskalering - sådanne AI’s findes der mange resultater af på nettet. I vores kode specifikt er det blot en ordinær forståelse af de allerede etablerede pixels, frem for en algoritmisk duplikation af de eksisterende pixels ved hjælp af store træningssæt.        

#### Hvilke linjer af kode har været særlig interessante for gruppen? Hvorfor?

``````

"Variable_1": {
   "filename": "Variable_1",
   "shape": [
     32
   ]
 },

``````

I Style Transfer Image er der lavet to model-mapper for hver af de to forskellige ‘stile’, som er lavet med j.son-filer som vi har beskæftiget os med før. Disse bliver brugt til at lave et stort `array` med forskellige variabler som tilsammen gør at programmet kan overføre stilen til de billeder vi selv har indsat. Ved at bruge j.son-filer kan man sortere de forskellige stilarter let og overskueligt. Det er samtidig et relativt enkelt element til at holde orden i en ellers kompliceret kode.


#### Hvordan ville I udforske/udnytte begrænsningerne af eksempel koden eller machine learning algoritmer?
I vores specifikke tilfælde, har det primært været spændende at se på hvad der sker, når vi ændrer i dataset, mængder af billeder programmet bruger og hvilken effekt det får på slutresultatet. “The great wave” og “young american girl, the dance” er de to billeder som i forvejen lå i ml5 biblioteket, altså “style”, som fungerer godt da de begge er meget stiliseret og har en meget markant effekt på billedet det arbejder med.  Et af de begrænsninger vi løb ind i, var da vi forsøgte at ændre i de styles som lå i ml5 biblioteket. Af hvert af billederne var der lavet 42 træningssæt, hvilket gjorde den bagved liggende “motor” så effektiv som den var. Hvis algoritmen skulle være effektiv, krævet det altså at koden har haft tilstrækkeligt med træningssæt.  

#### Var der syntakser/funktioner, som I ikke kendte til før? Hvilke? Og hvad lærte I?

``````
ml5.styleTransfer('models/wave')
 .then(style1 => style1.transfer(inputImg))
 .then(result => {
   const newImage1 = new Image(1000,1000);
   newImage1.src = result.src;
   styleA.appendChild(newImage1);
 });
``````

Denne funktion er funktionen der gør at billedet man har brugt som input bliver "style transferred". Der ses hvordan variablerne for billedet, hvis stil man gerne vil beholde, bliver kaldt og “lagt over” billedet man har brugt som input. Der bliver så oprettet, og defineret dimensionerne for størrelsen heraf og et nyt billede som er resultatet heraf bliver også skabt.
Noget denne syntaks visualiserer er hvordan koden for vildt komplekse programmer og funktioner stadig godt kan være korte. Kompleksiteten skabes ved alt det bagvedliggende og hvordan de forskellige syntakserne i koden refererer til dette.


#### Hvordan kan I se en større relation mellem eksempel koden, som I har undersøgt, og brugen af machine learning ude i verdenen (fx. kreative AI, Voice Assistance, selvkørende biler, bots, ansigtsgenkendelse osv.)?
 - spørgsmål besvares herunder
#### Hvad kunne du tænke dig at vide mere om? Eller hvilke spørgsmål kan du ellers formulere ift. emnet?

Som tidligere nævnt så har fokuset med netop vores AI været på den unikke menneskelige egenskab, kreativitet, samt AI’s emulering af samme egenskab. Dette trækker også tråde til spørgsmålet om ejerskab inden for AI. Hvilket gør sig gældende i de fleste former for AI’s hvis output består af en aduio-visuelt kunstværk. Det kunne derfor være yderligere spændende at kigge videre på hvilke andre AI’s der fremprovokere ligende ejerskabsproblematikker 
