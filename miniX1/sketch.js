// deklerer en variabel der beskriver mit billede til baggrund
let img;
// deklerer og initialiserer variabel for senere udformning af perlin noise algoritme
var xoff = 0;

// loader mit billede ind i koden 
function preload() {
  img = loadImage("./smerte.png");
}


// laver et canvas 
function setup() {
  createCanvas(400, 400);
 
}

function draw() {
  
 background(img);

 // tegner en ellipse og og tildeler den 1 dimensionel perlin noise på x aksen


var x = map(noise(xoff), 0, 1, 0, width);
xoff += 0.02;

 ellipse(x,200,24,24);
}

