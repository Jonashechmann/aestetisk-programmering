# miniX1 - Getting Started
Antal tegn = 4855

<img src="Skærmbillede_2023-02-10_kl._14.29.02.png">

[Link til at køre pogrammet](https://jonashechmann.gitlab.io/aestetisk-programmering/miniX1/index.html)



## Beskrivelse af programet 
Når man åbner koden vil man blive mødt af et 400x400 canvas som er en vittig illustration af glæden ved at programmere i p5.js. Den består helt basalt af en baggrund der er lavet i photoshop, kombineret med kode som får en cirkel til at bevæge sig langs en x-akse. Derudover kan man se at emoji's bruges til at illustrere følelsen bag at programmere som går fra trist til vred og sluttteligt glad. 

## Hvilke overvejlser gjorde jeg mig i kodningsprocessen
Jeg forsøgte mig indledningsvis med at bruge random syntaksen til at skabe en tilfeldig bevægelse langs canvas' x akse. Jeg var dog ikke tildfreds med den meget hakkede bevægelse som opstod som et resultat af de store vairationer i værdierne ved hver opdatering. Dette resulterede i at jeg i stedet måtte lede efter en ny måde, hvorpå jeg kunne lave en mere "glat" bevægelse langs x aksen, mens jeg sammentidig bibeholdte en tilfeldig variation. 

Her stødte jeg på "noise" syntaksen. Igennem research fandt jeg ud af at 3D noise ofte blev brugt til terrain generation i videospil, men at den også ofte blev benyttet til at lave 3D kunst. Jeg stødte her over en video fra codetrain hvori han forklarede hvordan 1D Perlin Noise kunne bruges til at skabe tilfeldige bløde variationer langs en x- eller y-akse - hvilket var præcis hvad jeg søgte. Jeg oplevede at Codingtrain var et rigtigt godt redskab, ikke kun til at finde den kode du manglede, men i høj ggrad også til at skabe en højere forståelse af den kode du valgte at bruge. Jeg lærte f.eks. den formel der lå til grunde for genereringen af perlin noise - hvilket i høj grad var med til at give mig en væstenlig mere brugbar forståelse af koden. Det betød at jeg kunne bruge og ændre i den kode som han præsenterede sådan at resultatet blev unikt, men også som jeg indledningsvist havde sat et mål om.  

Codingtrain hjalp mig altså meget med min forståelse for den kode jeg brugte, mens jeg blev overrasket over ChatGBT evner til at skabe utrolig specefik kode, som kunne lede mig på rette vej. Derudover var ChatGBT også virkelig god til debugging, da jeg kunne forklare hvorfor koden ikke virkede og den så ville retunere en mulig forklaring for hvorfor - i stedet for blot en løsning. Jeg endte dog slutteligt med at bruge en modificerede udgave af den kode jeg havde lært igennem codingtrain, da den passede bedre til hvad jeg håbede på. 

## Hvordan er kodning anderledes fra læsning og skrivning?
I starten var kodning meget kryptisk for mig og minede mig på ingen måde om normal skrivning eller læsning. Efter en masse resarch begyndte jeg dog at se mange ligheder. Kodning begyndte mere og mere at ligne at ordinært sprog. Noget så simpelt som opbygningen og den slaviske gennemgang af koden fra ende til ende var noget som, da jeg først forstod det, gav mig en meget bedre forsåelse for hvad jeg skulle gøre. Det gav mening at koden blev taget ende til ende og derefter blev læst igen - ligesom vis man skulle læse en række instruktioner flere gange.

## Hvordan kunne jeg bruge den læste litteratur til at fremme min forståelse for kodning. 
Meget af den tekst som vi har læst indtil videre handlede om hvordan kodning er ved at blive en essentiel del af samfundet på et globalt plan. Hvilket er noget, som jeg med min nu meget begrænsede erfaring med kode, virkelig godt kan forstå. Anemette Vee's tekst "Coding Literacy" bragte netop dette op. Tænker jeg tilbage på denne tekst har jeg en større forståelse for pointen om kodning som værende et essentielt sprog for hele befolkningen i fremtiden. Jeg ser nu store muligheder i en verden hvor de flest ehavde en basal kendskab til kode. Allered på først uge føler jeg mig langt mere en del af de hjemmesider, softwares og lignende ting som jeg benytter. 

De tekniske ting, så som opsætningen af GITLAB og VSC var også essentiele for at muliggøre udarbejdelsen af denne opgave samtidig med aat det muliggjorde udarbejdelsen af selve koden som jeg skabte.

## En kort gennemgang af min kod
```
// deklerer en variabel der beskriver mit billede til baggrund
let img;
// deklerer og initialiserer variabel for senere udformning af perlin noise algoritme
var xoff = 0;

// loader mit billede ind i koden 
function preload() {
  img = loadImage("./smerte.png");
}


// laver et canvas 
function setup() {
  createCanvas(400, 400);
 
}

function draw() {
 background(img);

 // tegner en ellipse og og tildeler den 1 dimensionel perlin noise på x aksen


var x = map(noise(xoff), 0, 1, 0, width);
xoff += 0.02;

 ellipse(x,200,24,24);

```

## Hvad synes jeg om kodning so far
Kodning var i starten meget skræmmende for mig og jeg følte at dette semester godt kunne blive et meget langt et. Jeg har dog efter første uge fået afkræftet denne teori. Kodning virker ikke længere så uoverskueligt som jeg færst havde frygtet og jeg har mmod på at lære meget mere i løbet af semestret. 


